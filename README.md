# Micro:bit experiments

Little micro:bit games and experiments done with my son

Code is imported in javascript from [Micro:bit Makecode](https://makecode.microbit.org/) blocks editor

# Little experiments

## Checks Jaime

[Checks Jaime](javascript/checks-jaime.js) used to count summer holidays daily tasks/exercises

[Test it in Makecode](https://makecode.microbit.org/_Cw9a7DCWY8Uw)

## Listillo

[Listillo](javascript/listillo.js) is a little game to guess a number between 0 and 10

[Test it in Makecode](https://makecode.microbit.org/_YLr5TWPW9Xam)

# License

GPL v3