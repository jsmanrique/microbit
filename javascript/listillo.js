let intentos = 0
let guess = 0
let item = 0
function Reset()  {
    item = Math.random(11)
    basic.showString("Listo!")
    guess = 0
    intentos = 0
}
input.onButtonPressed(Button.A, () => {
    guess += -1
    if (guess < 0) {
        guess = 0
    }
    basic.showNumber(guess)
})
input.onButtonPressed(Button.B, () => {
    guess += 1
    if (guess > 10) {
        guess = 10
    }
    basic.showNumber(guess)
})
input.onButtonPressed(Button.AB, () => {
    if (item == guess) {
        basic.showIcon(IconNames.Yes)
        music.beginMelody(music.builtInMelody(Melodies.Entertainer), MelodyOptions.Once)
        Reset()
    } else {
        basic.showIcon(IconNames.No)
        music.beginMelody(music.builtInMelody(Melodies.Wawawawaa), MelodyOptions.Once)
        intentos += 1
    }
    if (intentos == 5) {
        basic.showIcon(IconNames.Sad)
        music.beginMelody(music.builtInMelody(Melodies.Funeral), MelodyOptions.Once)
        basic.showString("Era el ...")
        basic.showNumber(item)
        Reset()
    }
})
Reset()