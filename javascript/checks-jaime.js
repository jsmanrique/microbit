let wrong = 0
let right = 0
function resetear()  {
    right = 0
    wrong = 0
    basic.showString("Ready!")
}
input.onButtonPressed(Button.A, () => {
    basic.showIcon(IconNames.Yes)
    music.beginMelody(music.builtInMelody(Melodies.BaDing), MelodyOptions.Once)
    right += 1
    basic.showNumber(right)
})
input.onButtonPressed(Button.B, () => {
    basic.showIcon(IconNames.No)
    music.beginMelody(music.builtInMelody(Melodies.Wawawawaa), MelodyOptions.Once)
    wrong += 1
    basic.showNumber(wrong)
})
input.onButtonPressed(Button.AB, () => {
    basic.showString("Right:")
    basic.showNumber(right)
    basic.showString("Wrong:")
    if (wrong == 0) {
        music.beginMelody(music.builtInMelody(Melodies.Entertainer), MelodyOptions.Once)
        basic.showString("Congrats!")
    }
    basic.showNumber(wrong)
})
input.onGesture(Gesture.Shake, () => {
    resetear()
})
resetear()